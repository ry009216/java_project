package uk.ac.reading.student.jmckeown;


import android.graphics.BitmapFactory;

import java.util.Random;

public class enyShip extends object {
    private int bulletThreshold;
    private int type;
    protected object bullet;
    private boolean fire;
    private int health;
    private int bulletFire;


    enyShip(float shipPosX, float shipPosY, int eType, GameView gameView) {
        super(shipPosX, shipPosY, 0, 0);
        type = eType;
        fire = false;
        setAttributes(type, gameView);

    }
    private void setAttributes(int type, GameView gameView)
    {
        Random rng = new Random();
        int direction = 1;
        if (rng.nextBoolean() == true)
        {
            direction = direction * -1;
        }
        else
        {
        }

        switch (type){

            case 0:
                bulletThreshold = 100;
                health = 3;
                Xspeed = (float) 100 * direction ;
                setSprite(BitmapFactory.decodeResource
                        (gameView.getContext().getResources(),
                                R.drawable.enmy));
                bulletFire = 2;
                break;
            case 1:
                bulletThreshold = 50;
                health = 1;
                Xspeed = (float) 150 * direction;
                setSprite(BitmapFactory.decodeResource
                        (gameView.getContext().getResources(),
                                R.drawable.enmy2));
                bulletFire = 1;
                break;
            case 2:
                bulletThreshold = 70;
                health = 5;
                Xspeed = (float) 75 * direction;
                Yspeed = (float) 40 ;
                setSprite(BitmapFactory.decodeResource
                        (gameView.getContext().getResources(),
                                R.drawable.enmy3));

                bulletFire = 3;
                break;
            case 3:
                bulletThreshold = 70;
                health = 5;
                Xspeed = (float) 75 * direction;
                Yspeed = (float) 20 ;
                setSprite(BitmapFactory.decodeResource
                        (gameView.getContext().getResources(),
                                R.drawable.enmy3));

                bulletFire = 3;
                break;


        }
    }

    public void bulletFire (float height, GameView screen)
    {
        bullet = new object(getX(), getY() + sprite.getHeight()/bulletFire, 0, height/bulletFire);
        bullet.setSprite((BitmapFactory.decodeResource
                (screen.getContext().getResources(),
                        R.drawable.bullet)));
        fire = true;

    }
    public void bulletHit()
    {
       // bullet = null;
        fire = false;
    }

    public int getThreshold()
    {
        return bulletThreshold;
    }
    public boolean getFire()
    {
        return fire;
    }
    public int getHealth()
    { return health;}
    public void setHealth(int nHp)
    {
        health = nHp;
    }
}
