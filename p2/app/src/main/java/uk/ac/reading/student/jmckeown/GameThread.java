package uk.ac.reading.student.jmckeown;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;

public abstract class GameThread extends Thread {
	//Different mMode states
	public static final int STATE_LOSE = 1;
	public static final int STATE_PAUSE = 2;
	public static final int STATE_READY = 3;
	public static final int STATE_RUNNING = 4;
	public static final int STATE_WIN = 5;
	public static final int STATE_HIT = 6;


	private float mX, mY;
	private static final float TOUCH_TOLERANCE = 4;
	private Path mPath;



	//Control variable for the mode of the game (e.g. STATE_WIN)
	protected int mMode = 1;

	//Control of the actual running inside run()
	private boolean mRun = false;
		
	//The surface this thread (and only this thread) writes upon
	private SurfaceHolder mSurfaceHolder;
	
	//the message handler to the View/Activity thread
	private Handler mHandler;
	
	//Android Context - this stores almost all we need to know
	private Context mContext;
	
	//The view
	public GameView mGameView;

	//We might want to extend this call - therefore protected
	protected int mCanvasWidth = 1;
	protected int mCanvasHeight = 1;

	//Last time we updated the game physics
	protected long mLastTime = 0;
 
	protected Bitmap mBackgroundImage;
	
	protected long score = 0;

	protected int lives = 0;

	protected int stage = 0;
	protected int finalStage = 5;

    //Used for time keeping
	private long now;
	private float elapsed;

    //Used to ensure appropriate threading
    static final Integer monitor = 1;

	protected String[] stageInfo;


	public GameThread(GameView gameView) {
		mGameView = gameView;

		mSurfaceHolder = gameView.getHolder();
		mHandler = gameView.getmHandler();
		mContext = gameView.getContext();
		
		mBackgroundImage = BitmapFactory.decodeResource
							(gameView.getContext().getResources(), 
							R.drawable.background);
		setStage(1);

	}
	
	/*
	 * Called when app is destroyed, so not really that important here
	 * But if (later) the game involves more thread, we might need to stop a thread, and then we would need this
	 * Dare I say memory leak...
	 */
	public void cleanup() {		
		this.mContext = null;
		this.mGameView = null;
		this.mHandler = null;
		this.mSurfaceHolder = null;
	}
	
	//Pre-begin a game
	abstract public void setupBeginning();
	
	//Starting up the game
	public void doStart() {
		synchronized(monitor) {
			
			setupBeginning();

			mLastTime = System.currentTimeMillis() + 100;

			setState(STATE_RUNNING);

			setScore(0);
			setLives(3);
		}
	}
	
	//The thread start
	@Override
	public void run() {
		Canvas canvasRun;
		while (mRun) {
			canvasRun = null;
			try {
				canvasRun = mSurfaceHolder.lockCanvas(null);
				synchronized (monitor) {
					if (mMode == STATE_RUNNING) {
						updatePhysics();
					}
					doDraw(canvasRun);
				}
			} 
			finally {
				if (canvasRun != null) {
					if(mSurfaceHolder != null)
						mSurfaceHolder.unlockCanvasAndPost(canvasRun);
				}
			}
		}
	}
	
	/*
	 * Surfaces and drawing
	 */
	public void setSurfaceSize(int width, int height) {
		synchronized (monitor) {
			mCanvasWidth = width;
			mCanvasHeight = height;

			// don't forget to resize the background image
			mBackgroundImage = Bitmap.createScaledBitmap(mBackgroundImage, width, height, true);
		}
	}

	protected void doDraw(Canvas canvas) {
		
		if(canvas == null) return;

		if(mBackgroundImage != null) canvas.drawBitmap(mBackgroundImage, 0, 0, null);
	}
	
	private void updatePhysics() {
		now = System.currentTimeMillis();
		elapsed = (now - mLastTime) / 1000.0f;

		updateGame(elapsed);

		mLastTime = now;
	}
	
	abstract protected void updateGame(float secondsElapsed);
	
	/*
	 * Control functions
	 */
	
	//Finger touches the screen
	public boolean onTouch(MotionEvent e) {
		if(e.getAction() == MotionEvent.ACTION_UP) {
			if (mMode == STATE_READY) {
				doStart();
				return true;
			} else if (mMode == STATE_PAUSE) {
				loadStage(stageInfo);
				unpause();
				return true;
			} else if (mMode == STATE_LOSE) {
				try{
					sleep(800);
					gameOver();

				}
				catch (Exception E)
				{

				}
				return true;

			} else if (mMode == STATE_WIN) {
				try{
					sleep(800);
					gameOver();

				}
				catch (Exception E)
				{

				}
				return true;
			} else if (mMode == STATE_HIT) {

				try {
					sleep(500);
					loadStage(stageInfo);
				} catch (Exception E) {

				}
				unpause();
				return true;
			}
		}
		else {
			synchronized (monitor) {
				float x = e.getX();
				float y = e.getY();
				switch (e.getAction()) {
					case MotionEvent.ACTION_DOWN:

						touchStart(x, y);

						break;
					case MotionEvent.ACTION_MOVE:
						touchStart(x, y);
						touchMove(x, y);
						break;

					case MotionEvent.ACTION_UP:

						break;

				}
			}
		}
		 
		return false;
	}

	public void loadStage(String[] stage)
	{

	}
	private void touchStart(float x, float y) {
		mPath = new Path();
		mPath.reset();
		mPath.moveTo(x, y);
		this.actionOnTouch(x, y);
		mX = x;
		mY = y;
	}

	private void touchMove(float x, float y) {
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);

		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
			this.actionOnTouch(x, y);
			mX = x;
			mY = y;
		}
		// issue h]ere void android.graphics.Path.quadTo(float, float, float, float)' on a null object reference on 227
	}

	protected void actionOnTouch(float x, float y) {
		//Override to do something
	}
	protected void onCreate(GameView screen)
	{

	}
	
	/*
	 * Game states
	 */
	public void pause() {
		synchronized (monitor) {
			if (mMode == STATE_RUNNING) setState(STATE_PAUSE);
		}
	}
	
	public void unpause() {
		// Move the real time clock up to now
		synchronized (monitor) {
			mLastTime = System.currentTimeMillis();
			setUpPause();
		}

		setState(STATE_RUNNING);
	}
	public void setUpPause(){

	}

	public void hit() {
		synchronized (monitor) {
			if (mMode == STATE_RUNNING) setState(STATE_HIT);
		}
	}



	//Send messages to View/Activity thread
	public void setState(int mode) {
		synchronized (monitor) {
			setState(mode, null);
		}
	}

	public void setState(int mode, CharSequence message) {
		synchronized (monitor) {
			mMode = mode;

			if (mMode == STATE_RUNNING) {
				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putString("text", "");
				b.putInt("viz", View.INVISIBLE);
				b.putBoolean("showAd", false);	
				msg.setData(b);
				mHandler.sendMessage(msg);
			} 
			else {				
				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				
				Resources res = mContext.getResources();
				CharSequence str = "";
				/*if (mMode == STATE_READY)
					str = res.getText(R.string.mode_ready);
				else 
					if (mMode == STATE_PAUSE)
						str = res.getText(R.string.mode_pause);
					else 
						if (mMode == STATE_LOSE)
							str = res.getText(R.string.mode_lose);
						else 
							if (mMode == STATE_WIN) {
								str = res.getText(R.string.mode_win);
							}

				 */

				if (message != null) {
					str = message + "\n" + str;
				}

				b.putString("text", str.toString());
				b.putInt("viz", View.VISIBLE);

				msg.setData(b);
				mHandler.sendMessage(msg);
			}
		}
	}
	
	/*
	 * Getter and setter
	 */	
	public void setSurfaceHolder(SurfaceHolder h) {
		mSurfaceHolder = h;
	}
	
	public boolean isRunning() {
		return mRun;
	}
	
	public void setRunning(boolean running) {
		mRun = running;
	}
	
	public int getMode() {
		return mMode;
	}

	public void setMode(int mMode) {
		this.mMode = mMode;
	}
	
	
	/* ALL ABOUT SCORES */
	
	//Send a score to the View to view 
	//Would it be better to do this inside this thread writing it manually on the screen?
	public void setScore(long score) {
		this.score = score;
		
		synchronized (monitor) {
			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putBoolean("score", true);
			b.putString("text", getScoreString().toString());
			msg.setData(b);
			mHandler.sendMessage(msg);
		}
	}

	public void gameOver()
	{
		synchronized (monitor) {
			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putBoolean("gameOver", true);
			msg.setData(b);
			mHandler.sendMessage(msg);
		}
	}

	public float getScore() {
		return score;
	}
	
	public void updateScore(long score) {
		this.setScore(this.score + score);
	}

	public void updateLives(int miss) {
		this.setLives(miss);
	}

	public void setLives(int lives2) {
		this.lives = lives + lives2;
		if (lives == 0)
		{
			setState(1);
		}
		synchronized (monitor) {
			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putBoolean("lives", true);
			b.putString("text", getLivesString().toString());
			msg.setData(b);
			mHandler.sendMessage(msg);
		}
	}

	public void updateStage(int Nstage) {
		this.setStage(Nstage);
	}

	public void setStage(int Nstage) {
		this.stage = Nstage;

		synchronized (monitor) {
			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putBoolean("stage", true);
			b.putString("text", getStageString().toString());
			msg.setData(b);
			mHandler.sendMessage(msg);
		}
		ChangeStage(stage);
	}

	protected CharSequence getLivesString() {
		return Integer.toString(this.lives);
	}

	protected CharSequence getStageString() {
		return Integer.toString(this.stage);
	}

	protected CharSequence getScoreString() {
		return Long.toString(Math.round(this.score));
	}

	private void ChangeStage(int tstage)
	{
		if (stageInfo != null)
		{
			stageInfo = null;
		}
			Resources res = mGameView.getResources();
			System.out.println("here " + tstage);
			switch (tstage) {
				case (1):
					stageInfo = res.getStringArray(R.array.stage1);
					break;

				case (2):
					stageInfo = res.getStringArray(R.array.stage2);
					break;

				case (3):
					stageInfo = res.getStringArray(R.array.stage3);
					break;
				case (4):
					stageInfo = res.getStringArray(R.array.stage4);
					break;
				case (5):
					stageInfo = res.getStringArray(R.array.stage5);
					break;

		}

	}


}

// This file is part of the course "Begin Programming: Build your first mobile game" from futurelearn.com
// Copyright: University of Reading and Karsten Lundqvist
// It is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// It is is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// 
// You should have received a copy of the GNU General Public License
// along with it.  If not, see <http://www.gnu.org/licenses/>.