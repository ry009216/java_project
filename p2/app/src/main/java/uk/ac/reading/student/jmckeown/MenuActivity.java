package uk.ac.reading.student.jmckeown;
import android.app.Activity;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;

public class MenuActivity extends Activity {
    private boolean pressed;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void onCreate(Bundle  savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        pressed = false;
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.menu_view);

        setup();
        setupButtons();

    }


     private void  setupButtons()
     {
         Button startGame = (Button) findViewById(R.id.startGame);
         Button levelSelect = (Button) findViewById(R.id.LevelSelect);
         Button Options = (Button) findViewById(R.id.Settings);

         startGame.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if (pressed == false) {
                     pressed = true;
                     Intent viewchange = new Intent(v.getContext(), MainActivity.class);
                     startActivity(viewchange);
                     System.out.println("test2");
                     finish();
                 }
             }
         });

         levelSelect.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if (pressed == false) {
                     pressed = true;
                     Intent viewchange = new Intent(v.getContext(), LevelSelect.class);
                     startActivity(viewchange);
                     System.out.println("test2");
                    // finish();
                 }
             }
         });
         Options.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if (pressed == false) {
                     pressed = true;
                     Intent viewchange = new Intent(v.getContext(), Settings.class);
                     startActivity(viewchange);
                     System.out.println("test3");
                     //finish();
                 }
             }
         });
     }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setup()
    {
        RelativeLayout lay = (RelativeLayout) findViewById(R.id.menuView);
        lay.setBackground(getDrawable(R.drawable.background));
        ImageView title = (ImageView) findViewById(R.id.title);
        title.setImageResource(R.drawable.title);
    }

}
