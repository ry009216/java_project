package uk.ac.reading.student.jmckeown;

import android.graphics.Bitmap;

public class object {
    protected Bitmap sprite;
    protected Float x;
    protected Float y;
    protected Float Xspeed;
    protected Float Yspeed;
    protected boolean alive;

    object(float shipPosX, float shipPosY,float canvasSizeX , float canvasSizeY)
    {
        x = shipPosX;
        y = shipPosY;
        Xspeed= canvasSizeX;
        Yspeed = canvasSizeY;
        alive = true;

    }
    public void setX(float px)
    {
        x = px;
    }
    public void setY(float py)
    {
        y = py;
    }
    public void setXSpeed(float pxs)
    {
        Xspeed = pxs;
    }
    public void setYSpeed(float pys)
    {
        Yspeed = pys;
    }
    public void setSprite(Bitmap sp)
    {
        sprite = sp;
    }



    public Float getX()
    {
        return x;
    }
    public Float getY()
    {
        return y;
    }
    public Float getXspeed()
    {
        return Xspeed;
    }
    public Float getYspeed()
    {
        return Yspeed;
    }


}
