package uk.ac.reading.student.jmckeown;

//Other parts of the android libraries that we use

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;

public class TheGame extends GameThread {

    private ship pShip;

    private int count= 0;

    private ArrayList<enyShip> enmySh;

    private ArrayList<object> bullets;

    private Bitmap BulletSprite;
    //This is run before anything else, so we can prepare things here
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public TheGame(GameView gameView) {
        //House keeping
        super(gameView);

        bullets = new ArrayList<object>();

        enmySh= new ArrayList<enyShip>();
        pShip = new ship(-100, -100, 0, 0);
        pShip.setSprite(BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.player));
        BulletSprite = (BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.bullet));

    }

    //This is run before a new game (also after an old game)
    @Override
    public void setupBeginning() {
      //  mMinDistanceBetweenBallAndPaddle2 = (pShip.sprite.getWidth() / 2 + bulletSprite.getWidth() / 2) * (pShip.sprite.getWidth() / 2 + bulletSprite.getWidth() / 2);
       // mMinDistanceBetweenShips = (enmySprite.getWidth() *100);
        loadStage(stageInfo);
        pShip.setX(mCanvasWidth/2);
        pShip.setY((float) (mCanvasHeight/1.15));
        pShip.alive = true;
        // fix this as to be used as the sprite hight work when collding into wall but not when used to check it colliding into each other. seems to be 100* less for ship collision
    }

    @Override
    public void loadStage(String[] stage)
    {
        bullets.clear();
        int numShip = Integer.parseInt(stage[0]);
        float [] tempX = new float[numShip];
        float [] tempY = new float[numShip];
        int [] tempT = new int[numShip];
        System.out.println( "size 1 " + tempX.length + "size 2 " + tempY.length + "size 3 " + tempT.length);
        for (int i = 0; i < numShip; i ++) {
                tempX [i] = mCanvasWidth/Float.parseFloat(stage [(1)+ (3 *i)]);
                tempY [i] = mCanvasHeight/Float.parseFloat(stage [(2)+ (3 *i)]);
                tempT [i] = Integer.parseInt(stage [(3)+ (3 *i)]);
        }
        setEnemies(numShip, tempX, tempY,tempT);
    }


    private void setEnemies(int numberSP, float[] x, float [] y, int [] type)
    {
        if (enmySh.size() != 0) {

            enmySh.clear();
        }
        for (int i = 0; i < numberSP; i ++)
        {
            enmySh.add(new enyShip( x[i],y[i] ,type[i], mGameView));
        }
    }

    @Override
    protected void doDraw(Canvas canvas) {

        if (canvas == null) return;

        super.doDraw(canvas);

        if (pShip.alive == true) {
            canvas.drawBitmap(pShip.sprite, pShip.getX() - pShip.sprite.getWidth() / 2, pShip.getY() - pShip.sprite.getHeight() / 2, null);
        }

        if (enmySh.size() > 0) {
            for (int j = 0; j < enmySh.size(); j++) {

                if (enmySh.get(j).alive == true) {

                    canvas.drawBitmap(enmySh.get(j).sprite, enmySh.get(j).getX() - enmySh.get(j).sprite.getWidth() / 2, enmySh.get(j).getY() - enmySh.get(j).sprite.getHeight() / 2, null);

                    if (enmySh.get(j).getFire() == true) {

                        canvas.drawBitmap(enmySh.get(j).bullet.sprite, enmySh.get(j).bullet.getX() - enmySh.get(j).bullet.sprite.getWidth() / 2,
                                enmySh.get(j).bullet.getY() - enmySh.get(j).bullet.sprite.getHeight() / 2, null);

                    }
                }
            }
        }


        if (bullets.size() > 0) {
            for (int i = 0; i < bullets.size(); i++) {
                if(bullets.get(i).alive == true) {

                    canvas.drawBitmap(BulletSprite, bullets.get(i).getX() - BulletSprite.getWidth() / 2, bullets.get(i).getY() - BulletSprite.getHeight() / 2, null);
                        //ava.lang.NullPointerException: Attempt to invoke virtual method 'int android.graphics.Bitmap.getWidth()' on a null object reference
                }
            }
        }
    }

    @Override
    protected void actionOnTouch(float x, float y) {
        //Move the ball to the x position of the touch
        double modifier = 1.5;
        double modifier2= 1.15;
        pShip.setX(x);
        if (y <= mCanvasHeight / modifier) {
            pShip.setY((float) (mCanvasHeight / modifier));
        }
        else if ( y >=mCanvasHeight/modifier2) {
            pShip.setY((float) (mCanvasHeight / modifier2));
        }
        else
            {
            pShip.setY(y);
        }
    }

    @Override
    protected void onCreate(GameView screen) {


        bullets.add(new object(pShip.getX(), (pShip.getY() - pShip.sprite.getHeight() / 3), (mCanvasWidth / 3), (-mCanvasHeight / 3)));
        bullets.get(bullets.size()-1).setSprite(BitmapFactory.decodeResource
                (screen.getContext().getResources(),
                        R.drawable.bullet));
    }


    //This is run just before the game "scenario" is printed on the screen

    protected void updateGame(float secondsElapsed) {

        //Move the paddle's X and Y using the speed (pixel/sec])
        pShip.setX(pShip.getX() + secondsElapsed * pShip.getXspeed());

        //If the ball goes out of the top of the screen and moves towards the top of the screen =>
       count += 1;
       if (count > 100)
       {
           count = 0;
       }

        updateBul(secondsElapsed);
        updateEn(secondsElapsed , count);

    }

    private void updateBul(float secondsElapsed) {
        if (bullets.size() != 0) {
            //ensures its not updating a null array
            for (int i = 0; i < bullets.size(); i++) {
                //loops for the amount of bullets on screen

                bullets.get(i).setY(bullets.get(i).getY() + secondsElapsed * bullets.get(i).getYspeed());

                for (int j = 0; j < enmySh.size(); j++) {

                    float minDistance = (enmySh.get(j).sprite.getWidth() / 2 + BulletSprite.getWidth() / 2) * (enmySh.get(j).sprite.getWidth() / 2 +  BulletSprite.getWidth() / 2);
                    if (updateBulletCollision(enmySh.get(j).getX(), enmySh.get(j).getY(), i , minDistance)) {

                        bullets.remove(i);
                        enmySh.get(j).setHealth(enmySh.get(j).getHealth() - 1);
                        if (enmySh.get(j).getHealth() == 0)
                        {
                            enmySh.remove(j);
                            updateScore(1);
                            checkEnemies();
                            break;
                        }
                        break;

                    }
                }
            }
        }
        checkbulcan();
    }

    private void checkbulcan ()
    {
        if (bullets.size() != 0)
        {
            for (int i = 0; i < bullets.size(); i++) {
                if (bullets.get(i).alive == true) {

                    if (bullets.get(i).getY() <= 0) {
                        bullets.remove(i);
                        break;
                    }
                }
            }
        }
    }
    private  void checkEnemies()
    {
        if (enmySh.size() < 1)
        {
            if (stage == finalStage)
            {
                setState(5);

            }
            else {
                updateStage(stage + 1);
                loadStage(stageInfo);
            }

        }
    }


    private void updateEn(float secondsElapsed, int count) {
        if (enmySh.size() != 0) {
            //ensures its not updating a null array
            for (int i = 0; i < enmySh.size(); i++) {
                //loops for the amount of bullets on screen
                enmySh.get(i).setY(enmySh.get(i).getY() + secondsElapsed * enmySh.get(i).getYspeed());
                enmySh.get(i).setX(enmySh.get(i).getX() + secondsElapsed * enmySh.get(i).getXspeed());

                for (int j = 0; j < enmySh.size(); j++) {
                    //Perform collisions (if necessary) between SadBall in position i and the red ball

                    if (i != j) {

                        float minDis = ((enmySh.get(i).sprite.getWidth() /2 + enmySh.get(j).sprite.getWidth() /2)* (enmySh.get(i).sprite.getWidth() /2 + enmySh.get(j).sprite.getWidth() /2));
                         updateEnemyCollision(enmySh.get(j).getX(), enmySh.get(j).getY(), i, minDis);
                    }

                }

                if (enmySh.get(i).getX() + enmySh.get(i).sprite.getWidth()/2  >= mCanvasWidth || enmySh.get(i).getX()  - enmySh.get(i).sprite.getWidth()/2 <= 0)
                {
                    enmySh.get(i).setXSpeed(enmySh.get(i).getXspeed() * -1);
                }
                if (enmySh.get(i).getY() >= mCanvasHeight || enmySh.get(i).getY() <= 0)
                {
                    enmySh.get(i).setYSpeed(enmySh.get(i).getYspeed() * -1);
                }

                if (count == enmySh.get(i).getThreshold() && enmySh.get(i).getFire() == false)
                {
                    enmySh.get(i).bulletFire(mCanvasHeight/3 , mGameView);
                }
                if (enmySh.get(i).getFire() == true)
                {
                    updateEnBull(secondsElapsed, i);
                }
            }
        }
    }

    private  void updateEnBull( float secondsElapsed, int i ) {
        if (enmySh.get(i).bullet.getYspeed() > 0) {

            //Check for a paddle collision
            if (updateEnBulletCollision(pShip.getX(), pShip.getY(), i) == true) {
                enmySh.get(i).bulletHit();
                updateLives(-1);
                hit();
                pShip.alive = false;
            }

        } else if (enmySh.get(i).bullet.getYspeed() < 0) {

            //Check for a paddle collision
            if (updateEnBulletCollision(pShip.getX(), pShip.getY(), i) == true) {
                enmySh.get(i).bulletHit();
                updateLives(-1);

            }

        }

        if (enmySh.get(i).getFire() == true) {

            enmySh.get(i).bullet.setY(enmySh.get(i).bullet.getY() + secondsElapsed * enmySh.get(i).bullet.getYspeed());

            if (enmySh.get(i).bullet.getY() <= 0) {
                enmySh.get(i).bulletHit();
            } else if (enmySh.get(i).bullet.getY() >= mCanvasHeight) {
                enmySh.get(i).bulletHit();
            }
        } else {

        }
    }

    private boolean updateEnBulletCollision(float x, float y, int i) {

        //Get actual distance (without square root - remember?) between the mBall and the ball being checked
        float distanceBetweenBulletandplayer = (x - enmySh.get(i).bullet.getX()) * (x - enmySh.get(i).bullet.getX()) + (y - enmySh.get(i).bullet.getY()) * (y - enmySh.get(i).bullet.getY());
        float MinDistanceBetweenbulletandplayer= (pShip.sprite.getWidth() / 2 + enmySh.get(i).bullet.sprite.getWidth() / 2) * (pShip.sprite.getWidth() / 2 + enmySh.get(i).bullet.sprite.getWidth() / 2);

        //Check if the actual distance is lower than the allowed => collision
        if ( MinDistanceBetweenbulletandplayer >= distanceBetweenBulletandplayer) {
            //Get the present speed (this should also be the speed going away after the collision)
            float objectSpeed = (float) Math.sqrt(enmySh.get(i).bullet.getXspeed() * enmySh.get(i).bullet.getXspeed() + enmySh.get(i).bullet.getYspeed() * enmySh.get(i).bullet.getYspeed());

            //Change the direction of the ball
            enmySh.get(i).bullet.setXSpeed( enmySh.get(i).bullet.getX() - x);
            enmySh.get(i).bullet.setYSpeed( enmySh.get(i).bullet.getY() - y);

            //Get the speed after the collision
            float newObjectSpeed = (float) Math.sqrt(enmySh.get(i).bullet.getXspeed() * enmySh.get(i).bullet.getXspeed() + enmySh.get(i).bullet.getYspeed() * enmySh.get(i).bullet.getYspeed());

            //using the fraction between the original speed and present speed to calculate the needed
            //velocities in X and Y to get the original speed but with the new angle.
            enmySh.get(i).bullet.setXSpeed(enmySh.get(i).bullet.getXspeed() * objectSpeed / newObjectSpeed);
            enmySh.get(i).bullet.setYSpeed(enmySh.get(i).bullet.getYspeed() * objectSpeed / newObjectSpeed);

            return true;
        }

        return false;


    }

    private boolean updateBulletCollision(float x, float y, int i, float mindis) {

        //Get actual distance (without square root - remember?) between the mBall and the ball being checked
        float distanceEnemyandBullet = (x - bullets.get(i).getX()) * (x - bullets.get(i).getX()) + (y - bullets.get(i).getY()) * (y - bullets.get(i).getY());

        //Check if the actual distance is lower than the allowed => collision
        if (mindis >= distanceEnemyandBullet) {
            //Get the present speed (this should also be the speed going away after the collision)
            float objectSpeed = (float) Math.sqrt(bullets.get(i).getXspeed() * bullets.get(i).getXspeed() + bullets.get(i).getYspeed() * bullets.get(i).getYspeed());

            //Change the direction of the ball
            bullets.get(i).setXSpeed(bullets.get(i).getX() - x);
            bullets.get(i).setYSpeed(bullets.get(i).getY() - y);

            //Get the speed after the collision
            float newObjectSpeed = (float) Math.sqrt(bullets.get(i).getXspeed() * bullets.get(i).getXspeed() + bullets.get(i).getYspeed() * bullets.get(i).getYspeed());

            //using the fraction between the original speed and present speed to calculate the needed
            //velocities in X and Y to get the original speed but with the new angle.
            bullets.get(i).setXSpeed(bullets.get(i).getXspeed() * objectSpeed / newObjectSpeed);
            bullets.get(i).setYSpeed(bullets.get(i).getYspeed() * objectSpeed / newObjectSpeed);

            return true;
        }

        return false;


    }

    private boolean updateEnemyCollision(float x, float y, int i, float mindis) {

        //Get actual distance (without square root - remember?) between the mBall and the ball being checked
        float distanceBetweenShip = (x - enmySh.get(i).getX()) * (x - enmySh.get(i).getX()) + (y - enmySh.get(i).getY()) * (y - enmySh.get(i).getY());

        //Check if the actual distance is lower than the allowed => collision
        if (mindis >= distanceBetweenShip) {
            //Get the present speed (this should also be the speed going away after the collision)
            float objectSpeed = (float) Math.sqrt(enmySh.get(i).getXspeed() * enmySh.get(i).getXspeed() + enmySh.get(i).getYspeed() * enmySh.get(i).getYspeed());

            //Change the direction of the ball
            enmySh.get(i).setXSpeed(enmySh.get(i).getX() - x);
            enmySh.get(i).setYSpeed(enmySh.get(i).getY() - y);

            //Get the speed after the collision
            float newObjectSpeed = (float) Math.sqrt(enmySh.get(i).getXspeed() * enmySh.get(i).getXspeed() + enmySh.get(i).getYspeed() * enmySh.get(i).getYspeed());

            //using the fraction between the original speed and present speed to calculate the needed
            //velocities in X and Y to get the original speed but with the new angle.
            enmySh.get(i).setXSpeed(enmySh.get(i).getXspeed() * objectSpeed / newObjectSpeed);
            enmySh.get(i).setYSpeed(enmySh.get(i).getYspeed() * objectSpeed / newObjectSpeed);

            return true;
        }

        return false;
    }

    @Override
    public void setUpPause()
    {
        pShip.alive = true;
        loadStage(stageInfo);
    }

};
// This file is part of the course "Begin Programming: Build your first mobile game" from futurelearn.com
// Copyright: University of Reading and Karsten Lundqvist
// It is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// It is is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// 
// You should have received a copy of the GNU General Public License
// along with it.  If not, see <http://www.gnu.org/licenses/>.
